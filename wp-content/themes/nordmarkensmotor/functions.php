<?php

add_action( 'wp_enqueue_scripts', 'jok_js', 11 );
function jok_js() {
	wp_enqueue_script( 'js_cycle2', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js', array('jquery') );
	wp_enqueue_script( 'js_backstretch', 'https://cdnjs.cloudflare.com/ajax/libs/danielgindi-jquery-backstretch/2.1.15/jquery.backstretch.min.js', array('jquery') );
	wp_enqueue_script( 'js_cookie_cookie', 'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js', array('jquery'), '2.1.2' );
	wp_enqueue_script( 'js_flex', 'https://cdnjs.cloudflare.com/ajax/libs/flexibility/2.0.1/flexibility.js', array('jquery') );
	wp_enqueue_script( 'jok_script_js', get_stylesheet_directory_uri().'/assets/js/jo-js.min.js', array( 'jquery' ), filemtime(get_stylesheet_directory() . '/assets/js/jo-js.min.js'), true);
	wp_enqueue_script( 'font-awesome', 'https://kit.fontawesome.com/0c14172326.js');
}

add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
function enqueue_child_theme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array('parent-style'), filemtime(get_stylesheet_directory() . '/style.css')  );
	wp_enqueue_style( 'jok-styles', get_stylesheet_directory_uri().'/assets/css/style.min.css',array(), filemtime(get_stylesheet_directory() . '/assets/css/style.min.css'));
}

/* ---- Add fonts ---- */

add_action( 'wp_print_styles', 'jok_fonts', 11);
function jok_fonts() {

}
// REMOVE ALL DIVI GOOGLE FONTS (Except Open Sans) FROM DIVI

// Add this code to enable divi fonts
//function et_builder_get_google_fonts() {return array();}
//function et_get_google_fonts() {return array();}


// REMOVE OPEN SANS GOOGLE FONT FROM DIVI
function disable_open_sans_divi() {
	wp_dequeue_style( 'divi-fonts' );
}
// Add this action to remove divi-fonts
//add_action( 'wp_enqueue_scripts', 'disable_open_sans_divi', 20 );

// REMOVE OPEN SANS GOOGLE FONT FROM MONARCH
function disable_open_sans_monarch() {
	wp_dequeue_style( 'et-gf-open-sans' );
}
add_action( 'wp_enqueue_scripts', 'disable_open_sans_monarch', 20 );

/* Remove Emojis */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/* Disable RSS Feeds */

function wpb_disable_feed() {
wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}

add_action('do_feed', 'wpb_disable_feed', 1);
add_action('do_feed_rdf', 'wpb_disable_feed', 1);
add_action('do_feed_rss', 'wpb_disable_feed', 1);
add_action('do_feed_rss2', 'wpb_disable_feed', 1);
add_action('do_feed_atom', 'wpb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wpb_disable_feed', 1);
add_action('do_feed_atom_comments', 'wpb_disable_feed', 1);
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version


/* ---- Add included files ---- */
add_action( 'after_setup_theme', 'jok_require_files');
function jok_require_files() {
	require_once( get_stylesheet_directory().'/includes/initiate-cpt.php');
	require_once( get_stylesheet_directory().'/includes/admin-menu.php');
	require_once( get_stylesheet_directory().'/includes/functions/theme-actions.php');
	require_once( get_stylesheet_directory().'/includes/functions/theme-filters.php');
	require_once( get_stylesheet_directory().'/includes/functions/theme-functions.php');
	require_once( get_stylesheet_directory().'/includes/functions/theme-shortcodes.php');
}





/* ---- Add Image sizes ---- */
add_action('after_setup_theme', 'jok_imagesizes');
function jok_imagesizes() {
	add_image_size('full-hd', 1920, 1080, false);
	add_image_size('jok-medium', 980, 980, false);
	add_image_size('jok-post', 400, 9999, false);
	add_image_size('jok-square', 500, 500, true);
}


add_action('after_setup_theme', 'jok_remove_imagesizes', 999);

function jok_remove_imagesizes() {
	remove_image_size('et-pb-post-main-image-fullwidth-large');
	remove_image_size('et-pb-portfolio-image-single');
	remove_image_size('et-pb-portfolio-module-image');
	remove_image_size('et-pb-portfolio-image');
}



/* ---- Add image size to editor ---- */

add_filter('image_size_names_choose', 'jok_image_sizes');
function jok_image_sizes($sizes) {
	$addsizes = array(
	"jok-medium" => __( "Medium-large"),
	"jok-square" => __( "Medium-square")
);
$newsizes = array_merge($sizes, $addsizes);
return $newsizes;
}


?>
