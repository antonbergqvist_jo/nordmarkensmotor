jQuery(document).ready(function($) {
    $('.settings_upload').click(function(e) {
        e.preventDefault();
		var that = $(this).attr('id');
        var custom_uploader = wp.media({
            title: 'Bild',
            button: {
                text: 'Välj/ladda upp'
            },
            multiple: false  // Set this to true to allow multiple files to be selected
        })
        .on('select', function() {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $('.settings_img'+'#'+that).attr('src', attachment.url);
            $('.settings_url'+'#'+that).val(attachment.url);
            $('.settings_id'+'#'+that).val(attachment.id);
        })
        .open();
    });
    $('.settings_remove').click(function(e) {
        e.preventDefault();
		var that = $(this).attr('id');
        $('.settings_img'+'#'+that).attr('src', 'https://dummyimage.com/254x143/9e9e9e/fff.jpg&text=x');
        $('.settings_url'+'#'+that).val('');
        $('.settings_id'+'#'+that).val('');
    });
});