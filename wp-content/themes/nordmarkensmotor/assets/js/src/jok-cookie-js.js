jQuery(document).ready(function ($) {
	if ((Cookies.get('cookie_info') != 'accept') && cookie_text) {
		var lightboxString = '<div class="jok-question cookie"><div class="jok-question-container cookie">';
		lightboxString += cookie_text.text;
		lightboxString += '<div class="jok-select et_pb_button" id="cookie"><span>';
		lightboxString += cookie_text.button;
		lightboxString += '</span></div></div></div>';
		$('body').append(lightboxString);
		$('.jok-question.cookie').fadeIn(400);
		$('.jok-select#cookie').on('click', function (e) {
			e.preventDefault();
			Cookies.set('cookie_info', 'accept', { expires: 720 });
			$('.jok-question.cookie').fadeOut(300);
		});
	}
});