function supportsFlexBox() {
    var test = document.createElement('test');

    test.style.display = 'flex';

    return test.style.display === 'flex';
}

if (supportsFlexBox()) {
    // Modern Flexbox is supported
} else {
    flexibility(document.documentElement);
}

function offsetAnchor() {
  if (location.hash.length !== 0) {
    window.scrollTo(window.scrollX, window.scrollY - 160);
  }
}

// Set the offset when entering page with hash present in the url
window.setTimeout(offsetAnchor, 0);

jQuery(document).ready(function($){
	// $('#main-content').css('min-height', $(window).height()-$('#top-header').outerHeight()-$('header#main-header').outerHeight()-$('footer#main-footer').outerHeight() -$('#wpadminbar').outerHeight() );
	// $( window ).resize(function() {
	// 	$('#main-content').css('min-height', $(window).height()-$('#top-header').outerHeight()-$('header#main-header').outerHeight()-$('footer#main-footer').outerHeight() -$('#wpadminbar').outerHeight() );
	// });
	$('.backstretch').each(function() {
		var align = ( $(this).data('backstretch-alignx') != null ? $(this).data('backstretch-alignx') : '0.5');
		var alignY = ( $(this).data('backstretch-aligny') != null ? $(this).data('backstretch-aligny') : '0.5');
		$(this).backstretch( $(this).data('backstretch-url'), {alignX: align, alignY: alignY} );
	});
	clickElem = null;
	// Set to standard jquery query-selector. all clicked items in selector expand and collapse according to css rules for classes expanding, collapsed and expanded.
	if ( clickElem ) {
		for (i = 0; i < clickElem.length; i++ ) {
			var clickedElem = clickElem[i];
			clickElem[i].addEventListener('click', function(e){
				var elem = document.getElementById('jok-mobile-menu');
			    if (elem.classList.contains('collapsed')) {
					expandCollapsed(elem);
				} else {
					collapseExpanded(elem);
				}
			});
		}
	}

	$(window).resize(function() {

		//adjust google map height
		if ($(window).width() > 980) {
			footerHeight = $('.footer-block').height();
			$('.et_pb_map_0_tb_footer>.et_pb_map').height(footerHeight);
		} else {
			$('.et_pb_map_0_tb_footer>.et_pb_map').height(400);
		}
	});

	$(window).scroll(function(){
		if ($(this).scrollTop() > 40) {
		   $('.homepage-header').addClass('scrolled');
		} else {
		   $('.homepage-header').removeClass('scrolled');
		}
	});
});

function expandCollapsed(elem) {
	var that = elem;

	// FIRST

	// Get initial size and position relative to the viewport
	var collapsed = elem.getBoundingClientRect();

	// LAST

	// Apply a class to control transition when element is expanding
	elem.classList.add('expanding');
	// Remove the initial class
	elem.classList.remove('collapsed');
	// Add the final class
	elem.classList.add('expanded');
	// Get final size and position relative to the viewport
	var expanded = elem.getBoundingClientRect();

	// INVERT

	// Save the values between intial state and final state
	// Use subtractions when manipulating positions to apply in transforms
	var invertedTop = collapsed.top - expanded.top;
	var invertedLeft = collapsed.left - expanded.left;
	// Use divisions when manipulating sizes to apply in scale
	var invertedWidth = collapsed.width / expanded.width;
	var invertedHeight = collapsed.height / expanded.height;

	// PLAY

	// Use transform origin to control the way the animation occurs
	elem.style.transformOrigin = 'top left';
	// Do the magic here, apply your saved values in transform style
	elem.style.transform = 'translate(' + invertedLeft + 'px, ' + invertedTop + 'px) scale(' + invertedWidth + ', ' + invertedHeight + ')'; 
	// Wait for the next frame so all the styles are changend
	requestAnimationFrame(function(){
		// Add the class to run the transition
		elem.classList.add('transition'); 
		// Clear styles
		elem.style.transform = '';
		var transitioned = function(){
			elem.style.transformOrigin = '';
			elem.classList.remove('transition');
			elem.classList.remove('expanding');
			// Remove the eventListener
			elem.removeEventListener('transitionend', transitioned);  
		}
		// On transitionEnd remove the classes used control transitions
		elem.addEventListener('transitionend', transitioned);
	
	}); 
}

function collapseExpanded(elem) {
    var that = elem;

	requestAnimationFrame(function(){

        that.classList.add('collapsing');
        that.classList.remove('expanded');
        that.classList.add('collapsed');
        var collapsed = that.getBoundingClientRect();
        that.classList.remove('collapsed');
        that.classList.add('expanded');
        var expanded = that.getBoundingClientRect();
        that.classList.add('transition');

        var invertedTop = collapsed.top - expanded.top;
        var invertedLeft = collapsed.left - expanded.left;
        var invertedWidth = collapsed.width / expanded.width;
        var invertedHeight = collapsed.height / expanded.height;

        that.style.transformOrigin = 'top left';
        that.style.transform = 'translate(' + invertedLeft + 'px, ' + invertedTop + 'px) scale(' + invertedWidth + ', ' + invertedHeight + ')';

        that.addEventListener('transitionend', function handler(event) {
            that.style.transform = '';
            that.style.transformOrigin = '';
            that.style.webkitTransform = '';
            that.style.webkitTransformationOrigin = '';
            that.classList.remove('transition');
            that.classList.remove('collapsing');
            that.classList.remove('expanded');
            that.classList.add('collapsed');
            that.removeEventListener('transitionend', handler);
        });

	});
}