require('es6-promise').polyfill();
require('isomorphic-fetch');
module.exports = function (grunt) {
	require("load-grunt-tasks")(grunt);
	grunt.initConfig({
		postcss: {
			options: {
				map: false,
				processors: [
					require('autoprefixer')({ browsers: ['> 0.5%', 'IE 8', 'IE 9', 'IE 10', 'iOS 8', 'iOS 9', 'Firefox ESR']})
				]
			},
			dist: {
				files: {
						'assets/css/style.css': 'assets/css/style.css'
				}
			}
		},
		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'assets/css',
		      src: ['style.css'],
		      dest: 'assets/css',
		      ext: '.min.css'
		    }]
		  }
		},
		babel: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					//"assets/js/jok-map.js": "assets/js/src/jok-rest.js"
				}
			}
		},
		sass: {
			dist: {
				files: {
				  'assets/css/style.css': 'assets/scss/main.scss'
		    	}
			}
		},
		uglify: {
			my_target: {
		    	files: {
						'assets/js/jok-js.min.js': ['assets/js/src/jok-js.js'],
						
						'assets/js/jok-cookie-js.min.js': ['assets/js/src/jok-cookie-js.js']
					} 
		    }
		},
		concat: {
		    options: {
		      separator: ';',
		    },
		    dist: {
					src: ['assets/js/jok-js.min.js', 'assets/js/jok-cookie-js.min.js'], 
		      dest: 'assets/js/jo-js.min.js',
		    },
		  },
		watch: {
			grunt: { files: ['Gruntfile.js'] },
				styles: {
					files: ['assets/scss/main.scss', 'assets/scss/extendables.scss', 'assets/scss/variables.scss', 'assets/scss/mixins.scss', 'assets/css/style.css', 'assets/js/src/jok-js.js', 'assets/js/src/jok-cookie-js.js', 'assets/js/src/jok-rest.js'],
					tasks: ['sass', 'postcss', 'babel', 'cssmin', 'uglify', 'concat']
				}
		}
    });
    grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default', ['copy', 'uglify', 'concat', 'babel', 'watch']);
};