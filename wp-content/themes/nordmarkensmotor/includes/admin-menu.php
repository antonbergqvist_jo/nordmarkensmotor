<?php
function jok_admin_style() {
       /* Register our stylesheet. */
       wp_register_style( 'jok_admin_style', get_stylesheet_directory_uri().'/assets/css/jok_admin_style.css' );
	   wp_register_script( 'jok-upload', get_stylesheet_directory_uri().'/assets/js/jok-upload.js', array('jquery','media-upload','thickbox'), filemtime( get_stylesheet_directory().'/assets/js/jok-upload.js' ), true );
   }  
   
add_action( 'admin_init', 'jok_admin_style' );


/* Add script to add media on custom settings page*/
function jok_options_enqueue_scripts() { 
	// Change or add page names for multiple instances
    wp_enqueue_script('jok-upload');
	wp_enqueue_style('jok_admin_style'); 
}
add_action('admin_enqueue_scripts', 'jok_options_enqueue_scripts');



// Add Admin Menu 
function jok_add() {

	if ( is_admin() ){

		function jok_admin() {
			include 'jok_admin.php';
			wp_enqueue_style('jok_admin_style'); 
		}

		add_action( 'admin_init', 'jok_settings_api_init' );
		add_action( 'admin_init', 'jok_plugin_settings' );

		add_action('admin_menu', 'jok_admin_actions');
		function jok_admin_actions() {
			add_menu_page('Custom theme settings', 'Custom theme settings', 'edit_theme_options', 'jok', 'jok_settings_page', false, 48 );
			add_submenu_page('jok', 'Custom theme settings', 'Custom theme settings', 'edit_theme_options', 'jok', 'jok_settings_page');
		}
	}
}

/* Add individual settings accessible by the wp_load_alloptions()-function or the get_option()-function */
function jok_plugin_settings() {
	/* --- Array of all settings, containing nested arrays */
	register_setting( 'jok_options', 'jok_options', 'jok_validation' );
}

function jok_validation($input) {
	global $option;
	if ( isset($_POST['cookie-info'] ) ) {
		$input['cookie_info'] = $_POST['cookie-info'];
	}
	return $input;
}


/*Add sections and setting fields */
function jok_settings_api_init() {
	add_settings_section('header-settings', 'Inställningar för headern', 'jok_header_section_cb', 'jok');

	add_settings_field( 'contact-info', 'Kontaktinformation', 'jok_settings_output_contact', 'jok', 'header-settings' );
	add_settings_field( 'standard-logo', 'Logotyp', 'jok_settings_output_logo', 'jok', 'header-settings' );	
	add_settings_field( 'cookie-info', 'Cookie-info', 'jok_settings_output_cookie_info', 'jok', 'header-settings');
}

/* Functions to output section information */
function jok_header_section_cb() {
	echo '<p>Inställningar för allmänna uppgifter, telefonnummer, logotyp och adressinformationen.</p>';
}


/*Functions to output field information */
function jok_settings_output_contact() {
	global $option;
	echo '<p>Epostadress</br><input type="text" name="jok_options[contact][email]" value="'.$option['contact']['email'].'" /></p>';
	echo '<p>Telefonnummer</br><input type="text" name="jok_options[contact][phone]" value="'.$option['contact']['phone'].'" /></p>';
	echo '<p>Telefonnummer, kompakt</br><input type="text" name="jok_options[contact][phone-compact]" value="'.$option['contact']['phone-compact'].'" /></p>';
	echo '<p>Copyright-text<br/><input type="text" name="jok_options[contact][copyright]" value="'.$option['contact']['copyright'].'" /></p>';
}


function jok_settings_output_logo() {
	global $option;
	?>
	<img id="logo_img" class="logo_img settings_img" src="<?php echo $option['logo']['image_url']; ?>"/><br/>
	<input id="logo_img" class="settings_id" type="hidden" name="jok_options[logo][image_id]" value="<?php echo $option['logo']['image_id']; ?>">
	<input id="logo_img" type="button" class="button settings_upload" value="Ladda upp/Välj"/>
	<input id="logo_img" type="button" class="button settings_remove" value="Rensa"/>
	<input id="logo_img" class="settings_url" type="hidden" name="jok_options[logo][image_url]" value="<?php echo $option['logo']['image_url']; ?>">
	<?php
}

function jok_settings_output_cookie_info() {
	global $option;
	echo '<p>Innehåll i cookie-info<br/>';
	wp_editor(stripslashes( $option['cookie_info'] ), 'cookie-info', array('textarea_rows' => '10') );
	echo '</p>';
	echo '<p>Text på knapp:<br/><input type="text" name="jok_options[cookie_button]" value="'.$option['cookie_button'].'"/></p>';
}

/*Function to output settingspage */

function jok_settings_page() {
	if(function_exists( 'wp_enqueue_media' )){
    		wp_enqueue_media();
	}else{
   		 wp_enqueue_style('thickbox');
    		wp_enqueue_script('media-upload');
    		wp_enqueue_script('thickbox');
	}
	global $option;
	wp_enqueue_style('jok_admin_style');
	echo '<div class="wrap">
		<h1>'.get_admin_page_title().'</h1> 
		<form method="post" action="options.php">';
   			settings_fields( 'jok_options' );
    			do_settings_sections( 'jok' );
    			submit_button();

		echo '</form>';
	echo '</div>';
}

?>