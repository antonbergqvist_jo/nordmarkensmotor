<?php
function jok_medarbetare_init() {
	$labels = array(
		'name'               => _x( 'Medarbetare', 'post type general name', 'jok' ),
		'singular_name'      => _x( 'Medarbetare', 'post type singular name', 'jok' ),
		'menu_name'          => _x( 'Medarbetare', 'admin menu', 'jok' ),
		'name_admin_bar'     => _x( 'Medarbetare', 'add new on admin bar', 'jok' ),
	);

	$args = array(
		'labels'             => $labels,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'public'	     => false,
		'show_ui'	     => true,
		'menu_icon'		 => 'dashicons-admin-users',
		'hierarchical'   => false,
		'rewrite'	     => array('slug' => 'medarbetare'),
		'register_meta_box_cb' => 'jok_meta_boxes',
		'supports'           => array( 'title', 'thumbnail', 'page-attributes', 'editor' )
	);

	register_post_type( 'medarbetare', $args );
}

add_action('init', 'jok_medarbetare_init');


add_action( 'init', 'jok_post_types');

function jok_post_types() {
	$labels = array(
			'name'               => _x( 'Bildspel', 'post type general name', 'your-plugin-textdomain' ),
			'singular_name'      => _x( 'Bild', 'post type singular name', 'your-plugin-textdomain' ),
			'menu_name'          => _x( 'Bildspel', 'admin menu', 'your-plugin-textdomain' ),
			'name_admin_bar'     => _x( 'Bildspel', 'add new on admin bar', 'your-plugin-textdomain' ),
			'add_new'            => _x( 'Lägg till ny', 'jok-slides', 'your-plugin-textdomain' ),
			'add_new_item'       => __( 'Lägg till ny bild', 'your-plugin-textdomain' ),
			'new_item'           => __( 'Ny bild', 'your-plugin-textdomain' ),
			'edit_item'          => __( 'Redigera bild', 'your-plugin-textdomain' ),
			'view_item'          => __( 'Se bild', 'your-plugin-textdomain' ),
			'all_items'          => __( 'Alla bilder', 'your-plugin-textdomain' ),
			'search_items'       => __( 'Sök bild', 'your-plugin-textdomain' ),
			'parent_item_colon'  => __( 'Överordnad bild:', 'your-plugin-textdomain' ),
			'not_found'          => __( 'Inga bilder hittade.', 'your-plugin-textdomain' ),
			'not_found_in_trash' => __( 'Inga bilder i skräpkorgen.', 'your-plugin-textdomain' )
		);

		$args = array(
			'labels'             => $labels,
            'description'        => __( 'Beskrivning.', 'your-plugin-textdomain' ),
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_icon'			 => 'dashicons-format-gallery',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'slide' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'register_meta_box_cb' => 'jok_meta_boxes',
			'supports'           => array( 'title', 'thumbnail', 'editor' )
		);

		register_post_type( 'jok-slides', $args );
		
}


function jok_register_taxonomies() {
	$labels = array(
		'name'              => _x( 'Tjänst', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Tjänst', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Sök tjänst', 'textdomain' ),
		'all_items'         => __( 'Alla tjänster', 'textdomain' ),
		'menu_name'         => __( 'Tjänster', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => false,
		'publicly_queryable'=> false,
		'query_var'         => true,
		'has_archive'		=> false,
		'rewrite'           => array( 'slug' => 'tjanst' ),
	);

	register_taxonomy( 'jok-service', array( 'medarbetare', 'jok-slides', 'jok-cases' ), $args );

}
		
add_action( 'init', 'jok_register_taxonomies');		

/* ---- Add meta-boxes ---- */

function jok_meta_boxes() {
	$views = array( 'jok-slides' );
	add_meta_box(
			'slide_target',
			__( 'Mål', 'jok' ),
			'target_callback',
			$views,
			'side'
		);
}



/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function target_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'target_save_meta_box_data', 'target_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, '_target_link', true );
	$value2 = get_post_meta( $post->ID, '_target_type', true);
	$page_target = get_post_meta( $post->ID, '_target_page', true);
	$value_text = get_post_meta( $post->ID, '_target_text', true);
	$pages = get_pages();
	echo '<p>';
	_e( 'Välj om innehållet ska länka till en befintlig sida eller en egen ifylld länk. Välj sedan sida eller fyll i en länk.', 'jok' );
	echo '</p>';
	echo '<select id="target_select" name="target_select">';
	echo '<option value="post"'.($value2=='post'? 'selected': '').'>Länka till det egna innehållet</option>';
	echo '<option value="page"'.($value2=='page'? 'selected': '').'>Länka till sida</option>';
	echo '<option value="link"'.($value2=='link'? 'selected': '').'>Använd egen länk</option>';
	echo '</select>';
	echo '<p>Fyll i text för ev. knapp: <br />';
	echo '<input type="text" id="target_text" name="target_text" value="'.$value_text.'"/>';
	echo '<p>Fyll i länken du vill använda:<br />';
	echo '<input type="text" id="target_link" name="target_link" value="'.($value2 == 'link' ? $value : '').'"></p>';
	echo '<p>Välj sida att använda<br />';
	echo '<select name="target_page" id="target_page">';
	echo '<option value="" '.(!$page_target ? 'selected': '').'></option>';
	foreach ($pages as $page) {
		echo '<option value="'.$page->ID.'" '.( $value2 == 'page' && $page_target == $page->ID ? 'selected': '').'>'.$page->post_title.'</option>';
	}
	echo '</select>';
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function target_save_meta_box_data( $post_id ) {
	$views = array(
		'jok-slides',
	);
	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['target_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['target_nonce'], 'target_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && in_array( $_POST['post_type'], $views) ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset( $_POST['target_select'] ) ) {
		return;
	}

	// Sanitize user input.
	$target = $_POST['target_select'];
	$link = $_POST['target_link'];
	$page = $_POST['target_page'];
	$text = $_POST['target_text'];

	// Update the meta field in the database.
	update_post_meta( $post_id, '_target_type', $target );
	update_post_meta( $post_id, '_target_link', $link);
	update_post_meta( $post_id, '_target_page', $page);
	update_post_meta( $post_id, '_target_text', $text);

}
add_action( 'save_post', 'target_save_meta_box_data' );

?>