<?php
// Unregister post type "Projects"
add_action('init', 'jok_unregister');

/* ---- Filter admin menu items ---- */
function custom_menu_page_removing() {
	$user = current_user_can('siteowner');
	if ( $user ) {
		$remove_items = array();
		$remove_items[] = 'et_divi_options';
		$remove_items[] = 'monsterinsights_dashboard';
		$remove_items[] = 'wpseo_dashboard';
		$remove_items[] = 'edit-comments.php';
		$remove_items[] = 'meowapps-main-menu';
		$remove_items[] = 'options-general.php';
		$remove_items[] = 'tools.php';
		$remove_items[] = 'profile.php';
		$remove_items[] = 'edit.php';
		$remove_items[] = 'loco';
		$remove_items[] = 'sat-order-sms-notification-settings';
		$remove_items[] = 'edit.php?post_type=jok-cookie';
		$remove_items[] = 'cff-top';
		$remove_items[] = 'et_divi_options';
		foreach( $remove_items as $menu_slug ) {

			remove_menu_page( $menu_slug );			
		}
	}

}
add_action( 'admin_menu', 'custom_menu_page_removing', 999 );

add_action('init', 'jok_theme_setup', 1);

add_action('init', 'jok_add', 3);
add_action( 'wp_head', 'jok_output_cookie', 1);
function jok_output_cookie() {
	$option = get_option('jok_options');
	if ( isset($option['cookie_info']) && $option['cookie_info'] != '' ) {
		?>
		<script type="text/javascript">
			var cookie_text = {
				text: <?php echo json_encode(wpautop(stripslashes( $option['cookie_info'] ))); ?>,
				button:'<?php echo $option['cookie_button']; ?>'
			};
		</script>
		<?php
	}
}
?>