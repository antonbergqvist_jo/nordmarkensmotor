<?php
/* ---- Unregister custom post type project ---- */

if ( ! function_exists( 'unregister_post_type' ) ) :
function unregister_post_type( $post_type ) {
    global $wp_post_types;
    if ( isset( $wp_post_types[ $post_type ] ) ) {
        unset( $wp_post_types[ $post_type ] );
        return true;
    }
    return false;
}
endif;

function jok_unregister() {
	unregister_post_type('project');
}


/* ---- Allow SVG ---- */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

function jok_theme_setup() {
	global $option;
	$option = get_option('jok_options');
}

?>