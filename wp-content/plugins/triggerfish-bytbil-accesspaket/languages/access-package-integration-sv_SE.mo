��    �      T      �      �  K   �     �     �     �     	     !	     4	     =	     L	     \	     e	  	   j	     t	  
   z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  "   �	     "
     &
     4
     A
     M
  
   b
     m
     z
     �
  	   �
     �
     �
  	   �
     �
     �
     �
     �
     �
          
               +  
   A     L     f  	   o     y     �  	   �     �     �  ,   �     �     �     �                           
   "     -     ?     L  
   S     ^     c     g     t     }     �     �  )   �     �     �     �  	   �  %   �               !     /     5     C     J  .   S     �     �     �     �     �     �     �     �     �     
           -     9     E     Q     ]     c  !   v     �      �     �     �  
   �  ^   �     E     M     ^     d     s     �  	   �     �     �     �     �     �     �  A   �     #     >     A  �  F  =   �          *     6     O  	   a  
   k     v     �  
   �     �  
   �     �     �     �     �  
   �     �             
                   %     +  &   F     m     q          �     �     �     �     �  	   �  
   �     �               (     1     ?     P     Y     e     q     }     �     �     �     �     �     �     �     �               #  )   1     [     a     f     y     �     �     �     �  
   �     �     �     �  
   �     �     �     �  	   �     �            )   "     L     U     e     t  %   �     �     �     �     �     �     �  	   �  3   �     $     2  
   ?     J     V     e     w     �     �     �  
   �     �  	   �     �  	   �            !        ?  &   M     t     �  	   �  Y   �     �     �               !     0     M  
   ]  
   h     s     {     �     �  C   �     �     �     �   <strong>Important!</strong> Only 4 filters will be shown in mobile devices. API settings Access package Activate DNB integration Address: %s, %s %s Administration fee All cars Archive Filter Arrangement fee Arrivals Back Body type Brand Buy online BytBil access package Bytbil Accesspaket CC Camper Car Car archive Car type Caravan City Color Content before car archive Content to show on single car page DNB DNB Dealer ID DNB settings Description Descriptions to show Directions Down payment Effective interest rate Email us Equipment Filters Filters to show Financing Font family Four wheel drive From %s kr/mon Fuel Gear Box Gear box Gearbox General General settings Hero background image Hero image Hide hero and all filters Interest JSON file Last 24 hours Last 48 hours Last week Leasing Length of vehicle Length of vehicle (caravan and campers only) Location Mail Mail settings Makes Milage Mile Mileage Model Model Year Model information Monthly cost Mopeds Motorcycle Name New New and used New cars New or used No Number of beds Number of beds (caravan and campers only) Old Only images Payback Phone: %s Plugin för att visa bilar till salu. Price Price Ex.moms Primary color Print Reduced price Reg.nr Residual Scroll to content after car on click on "Mail" Select an image Select brand Select city Select color Select fuel Select gearbox Select leasing Select length of vehicle Select mileage Select number of beds Select price Select sort Select type Select used Select year Share Show 4 latest cars Show filter under "more filters"? Show filter? Show form for "Intresseanmälan" Single car view Sort Text color The checked descriptions will be shown in each of the car cards displayed in the archive feed. Theming Theming settings Token Transport cars Triggerfish AB Use your own theme fonts Used cars Vehicle Type Vehicle type Version Warranty program Year Yes You can drag and drop the filters to get the order that you want. https://www.triggerfish.se kr mån Project-Id-Version: Bytbil Accesspaket
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-10-28 09:31+0000
PO-Revision-Date: 2020-10-28 09:32+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2 <strong>OBS!</strong> Endast 4 filter visas i mobila enheter. API-inställningar Accesspaket Aktivera DNB-integration Adress: %s, %s %s Aviavgift Alla bilar Arkivfilter Uppläggningsavgift Inkommande Tillbaka Fordonstyp Märke Köp online BytBil accesspaket BytBil accesspaket Motorvolym Husbil Bil Bilarkiv Fordonstyp Husvagn Stad Färg Innehåll före bilarkivet Innehåll att visa på enskild bilsida DNB DNB Dealer-ID DNB-inställningar Beskrivning Fordonsbeskrivningar som visas Vägbeskrivning Handpenning Effektiv ränta Maila oss Utrustning Filter Filtreringar som visas Finansering Typsnitt Fyrhjulsdrift Från %s kr/mån Bränsle Växellåda Växellåda Växellåda Allmänt Allmänna inställningar Hero-bakgrundsbild Herobild Dölj alla filter + hero Ränta JSON-fil 24 senaste timmarna 48 senaste timmarna Senaste veckan Leasing Fordonslängd Fordonslängd (endast husvagn och husbil) Plats Mail Mailinställningar Märken Miltal Miltal Miltal Modell Årsmodell Modellinformation Månadskostnad Mopeder Motorcykel Namn Ny Ny och begagnad Nya bilar Ny eller begagnad Nej Antal sängar Antal sängar (endast husbil och husvagn) Begagnad Endast med bild Återbetalning Telefon: %s Plugin för att visa bilar till salu. Pris Pris exkl. moms Primärfärg Skriv ut Nedsatt pris Reg. nr Restskuld Scrolla till block efter bilen vid klick på "Mail" Välj en bild Välj märke Välj stad Välj färg Välj bränsle Välj växellåda Välj leasing Välj fordonslängd Välj miltal Välj antal sängar Välj pris Välj sortering Välj typ Välj ny eller begagnad Välj år Dela Visa 4 senaste bilarna Visa filtret under "Fler filter"? Visa filtret? Visa formulär för "Intresseanmälan" Vy för enskild bil Sortera Textfärg De ikryssade beskrivningarna kommer att visas på varje bil-kort när det syns i arkivet. Tema Temainställningar Token Transportbilar Triggerfish AB Använd temats egna typsnitt Begagnade bilar Fordonstyp Fordonstyp Version Garantiprogram År Ja Du kan dra och släppa filtren i den ordning som du själv önskar. https://www.triggerfish.se kr mån 