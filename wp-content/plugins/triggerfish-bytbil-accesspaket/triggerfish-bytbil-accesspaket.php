<?php

/**
 * Plugin name: Bytbil Accesspaket
 * Author: Triggerfish AB
 * Author URI: https://www.triggerfish.se
 * Text Domain: access-package-integration
 * Version: 1.7.0
 * Description: Plugin för att visa bilar till salu.
 */

namespace TF\AccessPackage;

if (!defined('ABSPATH')) {
    exit;
}

define('TFAP_PLUGIN_VERSION', 1.6);

define('TFAP_PLUGIN_DIR', __DIR__);
define('TFAP_PLUGIN_URL', plugin_dir_url(__FILE__));
define('TFAP_PLUGIN_FILE', __FILE__);
define('TFAP_REACT_PATH', TFAP_PLUGIN_DIR . '/frontend');
define('TFAP_ASSET_MANIFEST', TFAP_REACT_PATH . '/build/asset-manifest.json');
define('TFAP_CLASSES', TFAP_PLUGIN_DIR . '/classes');
define('TFAP_ASSETS', plugins_url('assets', __FILE__));

require_once(TFAP_PLUGIN_DIR . '/classes/class-plugin.php');
require_once(TFAP_PLUGIN_DIR . '/sync/class-scheduler.php');

register_activation_hook(TFAP_PLUGIN_FILE, [Plugin::class, 'onActivation']);
register_deactivation_hook(TFAP_PLUGIN_FILE, [Plugin::class, 'onDeactivation']);

add_action('plugins_loaded', function () {
    Plugin::instance();
});
