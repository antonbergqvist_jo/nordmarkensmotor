<?php

namespace TF\AccessPackage;

class Mailer
{
    public function mail($request = '')
    {
        $to = $request->get_param('to');
        $msg = $request->get_param('msg');
        $url = $request->get_param('url');

        wp_mail("johan.moller@triggerfish.se", 'Kontaktformulär', 'hejhej');
        return [$to, $msg, $url];
    }
}
