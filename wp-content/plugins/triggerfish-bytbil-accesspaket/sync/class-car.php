<?php

namespace TF\AccessPackage\Sync;

class Car
{
    public static $keyExternalId = 'external_id';
    public static $postType = 'tf_car';
    public $postId;

    private $metaKeys = [
        'id',
        'enginePower',
        'gearBox',
        'fuel',
        'co2Emissions',
        'isEco',
        'vehicleType',
        'bodyType',
        'color',
        'freetextColor',
        'make',
        'model',
        'isModelApproved',
        'modelGroup',
        'modelRaw',
        'equipment',
        'modelYear',
        'regNo',
        'regDate',
        'regNoHidden',
        'hasImage',
        'isNew',
        'images',
        'warrantyProgram',
        'inWarrantyProgram',
        'price',
        'milage',
        'dealer',
        'accountManager',
        'carfaxReport',
        'additionalVehicleData',
        'beds',
        'length',
        'engineSize',
        'enginetype',
        'engineHours',
        'cargoLength',
        'cargoHeight',
        'cargoWidth',
        'totalWeight',
    ];

    private $car;

    public function __construct($car)
    {
        $this->car = $car;
    }

    public function syncCarToPost()
    {
        $metaKeys = $this->metaKeys;

        $content = array_filter($this->car, function ($carKey) use ($metaKeys) {
            return in_array($carKey, $metaKeys);
        }, ARRAY_FILTER_USE_KEY);

        $content['usedState'] = $content['isNew'] ? 'new' : 'old';
        $content['currentPrice'] = $content['price']['value'];
        $content['city'] = '';

        if (isset($content['dealer']['address']['city'])) {
            $content['city'] = ucfirst(mb_strtolower($content['dealer']['address']['city'], 'UTF-8'));
        }

        if (isset($content['images']) && !empty($content['images'])) {
            $content['thumbnailUrl'] = $content['images'][0]['imageFormats'][0]['url'];
        }

        $vehicle_color = $content['additionalVehicleData']['color'];
        if (isset($vehicle_color) && $vehicle_color != 'Okänd') {
            $content['color'] = $vehicle_color;
        }

        $vehicle_color_group = $content['additionalVehicleData']['colorGroup'];
        if (isset($vehicle_color_group) && !in_array($vehicle_color_group, ['', 'okänd', false, null])) {
            $content['colorGroup'] = ucfirst($vehicle_color_group);
        }

        $vehicle_fwd = $content['additionalVehicleData']['fourWheelDrive'];
        if (isset($vehicle_fwd) && $vehicle_fwd !== '') {
            $content['fourWheelDrive'] = true;
        }

        $content['leasing'] = !empty($content['price']['isLeasing']) ? '1' : '0';

        array_walk_recursive($content, function (&$value) {
            $value = str_replace("''", '"', $value);
            $value = addslashes($value);
        });

        $postData = [
            'post_title' => $this->car['name'],
            'post_type' => self::$postType,
            'post_status' => 'publish',
            'post_content' => json_encode($content, JSON_UNESCAPED_UNICODE),
            'post_date' => $this->getFormattedDate(),
        ];

        $this->postId = $this->checkIfCarExists();

        if (
            $this->postId &&
            $this->car['changedDate'] === get_post_meta($this->postId, 'changed_date', true)
        ) {
            return;
        }

        if (!$this->postId) {
            $this->postId = wp_insert_post($postData);
        } else {
            $postData['ID'] = $this->postId;
            wp_update_post($postData);
        }

        $this->addPostMeta();
    }

    private function checkIfCarExists()
    {
        $car = get_posts([
            'post_type' => self::$postType,
            'numberposts' => 1,
            'fields' => 'ids',
            'meta_key' => self::$keyExternalId,
            'meta_value' => $this->car['id'],
        ]);

        return $car ? $car[0] : false;
    }

    private function addPostMeta()
    {
        update_post_meta($this->postId, self::$keyExternalId, $this->car['id']);
        update_post_meta($this->postId, 'published_date', $this->car['publishedDate']);
        update_post_meta($this->postId, 'changed_date', $this->car['changedDate']);
    }

    private function getFormattedDate()
    {
        $gmt = new \DateTimeZone('GMT');
        $date_obj = new \DateTime($this->car['changedDate'], $gmt);
        $date_obj->setTimezone(new \DateTimeZone('Europe/Stockholm'));

        return $date_obj->format('Y-m-d H:i:s');
    }
}
