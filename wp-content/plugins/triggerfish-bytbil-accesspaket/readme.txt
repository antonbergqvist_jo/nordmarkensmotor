=== Bytbil accesspaket ===
Contributors: triggerfishab, moelleer, phpanos, sprazer
Tags: integration
Requires at least: 5.0
Tested up to: 5.3.2
Requires PHP: 7.3
Stable tag: 1.7.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugins enables the resellers of BytBil to get their feed displayed on their site.

== Description ==

This plugins enables the resellers of BytBil to get their feed displayed on their site.

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `access-package-integration/` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Enter your credentials on the settings page "Settings > Access package"

== Changelog ==
= 1.7 =
Email form on single car pages

= 1.6 =
Single car pages when embedded external

= 1.5 =
* Allow external use of plugin.

= 1.4 =
* DNB Integration

= 1.1 =
* Minor design changes.

= 1.0 =
* First release with archive and single car views.

== Requirements ==
* PHP 7.3+
