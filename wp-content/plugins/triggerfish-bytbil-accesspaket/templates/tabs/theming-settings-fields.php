<tr valign="top">
    <th scope="row"><?php echo esc_html__('Primary color', 'access-package-integration'); ?></th>
    <td>
        <input
            type="text"
            class="tfap-color-field"
            name="access_package_primary_color"
            value="<?php echo esc_attr(get_option('access_package_primary_color')); ?>" />
    </td>
</tr>

<tr valign="top">
    <th scope="row"><?php echo esc_html__('Text color', 'access-package-integration'); ?></th>
    <td>
        <input
            type="text"
            class="tfap-text-color-field"
            name="access_package_text_color"
            value="<?php echo esc_attr(get_option('access_package_text_color')); ?>" />
    </td>
</tr>

<tr valign="top">
    <th scope="row"><?php echo esc_html__('Font family', 'access-package-integration'); ?></th>
    <td>
        <?php
        $tfap_google_fonts = [
            'Lato',
            'Montserrat',
            'Open Sans',
            'Oswald',
            'PT Sans',
            'Raleway',
            'Roboto Condensed',
            'Roboto',
            'Slabo 27px',
            'Source Sans Pro',
        ];
        $selected_font = get_option('access_package_font_family');
        ?>
        <select class="tfap-font-family-field" name="access_package_font_family">
            <option value="inherit" <?php selected($options['select_field_0'], 1); ?>>
                <?php esc_html_e('Use your own theme fonts', 'access-package-integration'); ?>
            </option>
            <?php foreach ($tfap_google_fonts as $font) : ?>
                <option value="<?php echo $font; ?>" <?php echo $selected_font === $font ? 'selected' : ''; ?>>
                    <?php echo $font; ?>
                </option>
            <?php endforeach; ?>
        </select>
    </td>
</tr>
