<?php

if (!defined('ABSPATH')) {
    exit;
}
?>

<div style="<?php echo esc_attr($hide_content); ?>">
    <h2><?php esc_html_e('Filters to show', 'access-package-integration'); ?></h2>
    <p>
        <?php
        esc_html_e(
            'You can drag and drop the filters to get the order that you want.',
            'access-package-integration'
        );
        ?>
    </p>
    <p>
        <?php
        _e(
            '<strong>Important!</strong> Only 4 filters will be shown in mobile devices.',
            'access-package-integration'
        );
        ?>
    </p>

    <table class="form-table filter-table" style="width: auto;">
        <thead style="border-bottom: 1px solid #ccc">
            <tr>
                <th><?php _e('Name', 'access-package-integration'); ?></th>
                <th><?php _e('Show filter?', 'access-package-integration'); ?></th>
                <th><?php _e('Show filter under "more filters"?', 'access-package-integration'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (\TF\AccessPackage\Filters::sortedFilters() as $filter_field) : ?>
                <tr valign="top" class="filter-item" style="cursor: grab; border-bottom: 1px solid #ccc;">
                    <th scope="row"><?php echo esc_html($filter_field['label']); ?></th>
                    <th>
                        <input type="checkbox" name="<?php echo esc_attr($filter_field['field']); ?>" value="1" <?php echo checked(get_option($filter_field['field'])); ?> />
                    </th>
                    <th>
                        <?php $input_name = sprintf('%s_show_more', esc_attr($filter_field['field'])); ?>
                        <input type="checkbox" name="<?php echo $input_name; ?>" value="1" <?php echo checked(get_option($input_name)); ?> />
                    </th>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
