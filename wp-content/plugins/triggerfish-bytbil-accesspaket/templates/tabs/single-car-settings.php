<?php

if (!defined('ABSPATH')) {
    exit;
}
?>

<div style="<?php echo esc_attr($hide_content); ?>">
    <h2><?php esc_html_e('Content to show on single car page', 'access-package-integration'); ?></h2>

    <table class="form-table description-table">
        <tr valign="top" class="description-item">
            <td>
                <textarea
                    name="access_package_single_car_content"
                    id="access_package_single_car_content"
                    rows="12"
                    class="js-access-package-single-car-content"
                    style="width: 100%;"
                >
                    <?php echo get_option('access_package_single_car_content'); ?>
                </textarea>
            </td>
        </tr>
    </table>
</div>
