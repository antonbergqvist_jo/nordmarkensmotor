<?php

if (!defined('ABSPATH')) {
    exit;
}
?>

<?php get_header(); ?>

<?php $content = get_post_meta(get_the_ID(), 'tfap_content_before_filters', true); ?>

<?php if ($content) : ?>
    <div class="tfap-container tfap-no-print">
        <div class="tfap-row tfap-justify-center">
            <div class="tfap-col-10 tfap-car-content" style="padding: 70px 0; width: 83.3333%">
                <div class="entry-content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div id="tfap-root" class="tfap-app" style="padding-bottom: 70px;"></div>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : ?>
        <?php the_post(); ?>
        <div class="tfap-container tfap-no-print">
            <div class="tfap-row tfap-justify-center">
                <div class="tfap-col-10 tfap-car-content" style="padding-bottom: 70px; width: 83.3333%">
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();
