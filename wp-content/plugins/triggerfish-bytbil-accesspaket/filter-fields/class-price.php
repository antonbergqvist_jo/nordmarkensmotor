<?php

namespace TF\AccessPackage\FilterFields;

class Price
{
    public function data($post_id = '')
    {
        return [
            'filterKey' => 'currentPrice',
            'title' => esc_html__('Price', 'access-package-integration'),
            'adminTitle' => esc_html__('Price', 'access-package-integration'),
            'field' => 'access_package_filter_show_price',
            'order' => \TF\AccessPackage\Filters::filterOrder('access_package_filter_show_price'),
            'type' => 'range',
            'placeholder' => esc_html__('Select price', 'access-package-integration'),
            'values' => self::values(),
        ];
    }

    private static function values()
    {
        $price_range = array_merge(range(10000, 100000, 10000), range(120000, 1000000, 20000));
        return array_map(function ($price) {
            return [
                'value' => $price,
                'label' => $price,
            ];
        }, $price_range);
    }
}
