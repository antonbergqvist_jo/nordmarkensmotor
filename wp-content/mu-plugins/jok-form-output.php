<?php
/*
Plugin Name: Jo Kommunikation Form Output
Description: Adds capability to display a shortcode in either a side-panel or a lightbox
Author: Jo Kommunikation AB
Version: 0.1
Author URI: http://jokommunikation.se
*/



add_action('init', 'joform_theme_setup', 1);
add_action('init', 'joform_options', 4);

add_action( 'wp_enqueue_scripts', 'enqueue_joform_styles', PHP_INT_MAX);
function enqueue_joform_styles() {
	wp_enqueue_style( 'joform-styles', plugins_url('/jok-form/assets/css/form-style.min.css', __FILE__), array(), filemtime(plugin_dir_path(__FILE__).'/jok-form/assets/css/form-style.min.css'));
}

add_action( 'wp_enqueue_scripts', 'joform_js', 11 );
function joform_js() {
    wp_enqueue_script( 'joform_js', plugins_url('/jok-form/assets/js/joform-js.min.js', __FILE__), array( 'jquery' ), filemtime(plugin_dir_path(__FILE__).'/jok-form/assets/js/joform-js.min.js'), true);
}

function joform_theme_setup() {
	global $joform_option;
	$joform_option = get_option('joform_options');
}

/*
* Add validation of form and if so trigger open form field.
*
*/
function joform_filter_side_form_validation($validation_result) {
    global $joform_option;
    $valid = true;
    $form = $validation_result['form'];
    foreach( $form['fields'] as &$field ) {
        if ( $is_valid ) {
            continue;
        } else {
            if ( $valid ) {
                $valid = false;
            }
        }
    }
    if (!$valid ) $joform_option['form_validation'] = false;
    return $validation_result;
}

add_filter('gform_validation', 'joform_filter_side_form_validation');


add_action( 'wp_footer', 'joform_form_output');

if ( !function_exists('joform_form_output') ) {
    function joform_form_output() {
        global $option;
        global $joform_option;
        // Check if form should be output at all.
        if ( isset($joform_option['display']) ) {
            if ( $joform_option['display'] == 'true' ) {
                $valid = true;
                // Checks for validation errors thrown by our theme
                if ( isset($option['form_validation']) && $option['form_validation'] != true ) {
                    $valid = false;
                } else if ( isset($joform_option['form_validation'] ) && $joform_option['form_validation'] != true ) {
                    $valid = false;
                }
                if ( function_exists('pll_current_language') ) {
                    if ( ( pll_current_language() == 'en' ) ) {
                        $joform_option['button_text'] = $joform_option['eng']['button_text'];
                        $joform_option['default_form'] = $joform_option['eng']['default_form'];
                    }
                }
                if ( defined('ICL_LANGUAGE_CODE') ) {
                    if ( ( ICL_LANGUAGE_CODE == 'en' ) ) {
                        $joform_option['button_text'] = $joform_option['eng']['button_text'];
                        $joform_option['default_form'] = $joform_option['eng']['default_form'];
                    }
                }
                ob_start();
                if ( $joform_option['form_type'] == 'lightbox' ) {
                    ?>
                    <div class="joform_container joform_container--lightbox joform_container--closed<?php echo ( $valid ? '' : " joform_container--open" ) ?>">
                        <div class="joform_container__form">
                            <?php echo do_shortcode($joform_option['default_form']); ?>
                            <div class="joform_container__close"><i class="fal fa-times-square"></i></div>
                        </div>
                    </div>
                    <div class="joform_sidetoggle">
                        <?php echo '<h4>'.$joform_option['button_text'].'</h4>'; ?>
                    </div>
                    <?php
                } else {
                    echo '<div class="joform_container joform_container--side'.( isset($option['form_validation']) && !$option['form_validation'] ? " joform_container--open" : '' ).'">';
                    echo '<div class="joform_container__close"><i class="fal fa-times-square"></i></div>';
                    echo do_shortcode($joform_option['default_form']);
                    echo '<div class="joform_container__tag">';
                    echo '<h4>'.$joform_option['button_text'].'</h4>';
                    echo '</div>';
                    echo '</div>';
                }
                echo ob_get_clean();
            }
        }
    }
}

// Add Admin Menu 
function joform_options() {

	if ( is_admin() ){

		function joform_admin() {
			include 'joform_admin.php';
			wp_enqueue_style('joform_admin_style'); 
		}

		add_action( 'admin_init', 'joform_settings_api_init' );
		add_action( 'admin_init', 'joform_plugin_settings' );

		add_action('admin_menu', 'joform_admin_actions');
		function joform_admin_actions() {
            if ( $GLOBALS['admin_page_hooks']['jok'] ) {
                add_submenu_page('jok', 'Formulär - Inställningar', 'Formulär - Inställningar', 'edit_theme_options', 'joform', 'joform_settings_page');
            } else {
                add_menu_page('Formulär - Inställningar', 'Formulär - Inställningar', 'edit_theme_options', 'joform', 'joform_settings_page', false, 48 );
                add_submenu_page('joform', 'Formulär - Inställningar', 'Formulär - Inställningar', 'edit_theme_options', 'joform', 'joform_settings_page');
            }
		}
	}
}

/* Add individual settings accessible by the wp_load_alloptions()-function or the get_option()-function */
function joform_plugin_settings() {
	/* --- Array of all settings, containing nested arrays */
    register_setting( 'joform_options', 'joform_options', 'joform_validation_cb');
}

/*Add sections and setting fields */
function joform_settings_api_init() {
    add_settings_section('form-settings', 'Inställningar för formuläret', 'joform_form_section_cb', 'joform');
    

	add_settings_field( 'form-info', 'Formulärinställningar', 'joform_form_settings', 'joform', 'form-settings' );
}

/* Functions to output section information */
function joform_form_section_cb() {
	echo '<p>Inställningar för allmänna uppgifter för formuläret.</p>';
}

function joform_form_settings() {
    global $joform_option;
    echo '<p><b>Ska formuläret visas?</b>: <input type="checkbox" name="joform_options[display]" value="true" '.($joform_option["display"] == 'true' ? 'checked=checked' : '' ).'">Ja';
    echo '<p>Text på knapp: <input type="text" name="joform_options[button_text]" value="'.$joform_option["button_text"].'"/>';
    // Check for multi language sites, using Polylang and WPML as plugins
    if ( function_exists('pll_current_language') || function_exists('icl_object_id') ) {
        echo '<p>Text på knapp, engelska: <input type="text" name="joform_options[eng][button_text]" value="'.$joform_option['eng']["button_text"].'"/>';
    }
    echo '<p>Shortcode för formulärutmatning: <input type="text" name="joform_options[default_form]" value="'.$joform_option["default_form"].'"/>';
    // Check for multi language sites, using Polylang and WPML as plugins
    if ( function_exists('pll_current_language') || function_exists('icl_object_id') ) {
        echo '<p>Shortcode för formulärutmatning, engelska: <input type="text" name="joform_options[eng][default_form]" value="'.$joform_option["eng"]["default_form"].'"/>';
    }
    echo '<p>Formulärtyp:</br><label for="form_type_slide">Sidoformulär</label><input id="form_type_slide" type="radio" name="joform_options[form_type]" value="slide"'.( $joform_option["form_type"] == 'slide' ? ' checked="checked"' : '' ).'>';
    echo '</br><label for="form_type_lightbox">Lightbox</label><input id="form_type_lightbox" type="radio" name="joform_options[form_type]" value="lightbox"'.( $joform_option["form_type"] == 'lightbox' ? ' checked="checked"' : '' ).'></p>';
}

/*Function to output settingspage */

function joform_settings_page() {
	if(function_exists( 'wp_enqueue_media' )){
    		wp_enqueue_media();
	}else{
   		 wp_enqueue_style('thickbox');
    		wp_enqueue_script('media-upload');
    		wp_enqueue_script('thickbox');
	}
	global $option;
	wp_enqueue_style('joform_admin_style');
	echo '<div class="wrap">
		<h1>'.get_admin_page_title().'</h1> 
		<form method="post" action="options.php">';
   			settings_fields( 'joform_options' );
    			do_settings_sections( 'joform' );
    			submit_button();

		echo '</form>';
	echo '</div>';
}


?>