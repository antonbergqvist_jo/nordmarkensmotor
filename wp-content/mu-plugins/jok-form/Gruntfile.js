module.exports = function (grunt) {
    grunt.initConfig({
        autoprefixer: {
			options: {
				map: false,
				browsers: ['> 0.5%', 'IE 8', 'IE 9','IE 10', 'iOS 8', 'iOS 9']
			},
            dist: {
                files: {
                    'assets/css/form-style.css': 'assets/css/form-style.css'
                }
            }
        },
		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'assets/css',
		      src: ['form-style.css'],
		      dest: 'assets/css',
		      ext: '.min.css'
		    }]
		  }
		},
		sass: {
			dist: {
				files: {
				  'assets/css/form-style.css': 'assets/scss/form-styles.scss'
		    	}
			}
		},
		uglify: {
			my_target: {
		    	files: {
					'assets/js/joform-js.min.js': ['assets/js/joform-js.js'],
		      	}
		    }
		},
		concat: {
		    options: {
		      separator: ';',
		    },
		    dist: {
		      src: ['assets/js/jok-js.min.js', 'assets/js/jok-cookie-js.min.js'],
		      dest: 'assets/js/jo-js.min.js',
		    },
		  },
        watch: {
			grunt: { files: ['Gruntfile.js'] },
            styles: {
                files: ['assets/scss/form-styles.scss', 'assets/css/form-style.css', 'assets/js/joform-js.js'],
                tasks: ['sass', 'autoprefixer', 'cssmin', 'uglify']
            }
        }
    });
    grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default', ['copy', 'uglify', 'concat', 'watch']);
};