jQuery(document).ready(function ($) {
    $(document).on('click', ' .form-open, .joform_container__close, .joform_sidetoggle, .joform_container__tag', function (event) {
        event.preventDefault();
        $('.joform_container').toggleClass('joform_container--open');
    });
    $(document).on('click', '.joform_container', function (event) {
        if (event.target.classList.contains('joform_container--open')) {
            $('.joform_container').toggleClass('joform_container--open');
        }
    });
});